import React from "react";
import { PressableProps, StyleProp, ViewStyle } from "react-native";
interface Props extends PressableProps {
    bgs?: any;
    style?: StyleProp<ViewStyle>;
}
declare const TouchNaNa: ({ bgs, style, ...rest }: Props) => React.JSX.Element;
export default TouchNaNa;
//# sourceMappingURL=TouchNaNa.d.ts.map