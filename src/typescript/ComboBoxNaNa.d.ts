import React from "react";
import { ViewProps } from "react-native";
export interface ComboBoxNaNaProps extends ViewProps {
    size?: any;
    ratio?: any;
    color?: any;
    stretch?: boolean;
    cover?: boolean;
    contain?: boolean;
    size_14?: boolean;
    size_16?: boolean;
    size_20?: boolean;
    size_24?: boolean;
    size_28?: boolean;
    size_32?: boolean;
    size_36?: boolean;
    size_40?: boolean;
}
declare const _default: {
    Chip: ({ title, value, onPress, ...rest }: any) => React.JSX.Element;
    Drop: ({ title, value, onPress, ...rest }: any) => React.JSX.Element;
    Select: ({ title, disabled, isSelect, onPress, style, ...rest }: any) => React.JSX.Element;
};
export default _default;
//# sourceMappingURL=ComboBoxNaNa.d.ts.map