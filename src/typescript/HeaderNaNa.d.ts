import React from 'react';
interface IconProps {
    type?: 'back' | 'setting' | 'notification' | 'info';
    onPress?: Function;
}
export interface HeaderPropsType {
    title?: string;
    navigation?: any;
    rightChild?: any;
    leftChild?: any;
    left?: IconProps;
    right?: IconProps;
    line?: any;
}
declare const HeaderNaNa: ({ leftChild, line, rightChild, title, right, left }: HeaderPropsType) => React.JSX.Element;
export default HeaderNaNa;
//# sourceMappingURL=HeaderNaNa.d.ts.map