import React from "react";
import { ViewAppProps } from "react-native-package-fast";
interface Props extends ViewAppProps {
    title?: any;
    des?: any;
    icon?: any;
    iconView?: any;
}
declare const _default: {
    Nor: ({ title, des, children, iconView, icon, ...rest }: Props) => React.JSX.Element;
    Small: ({ title, des, iconView, icon, ...rest }: Props) => React.JSX.Element;
};
export default _default;
//# sourceMappingURL=EmptyNaNa.d.ts.map