import React from 'react';
import { TouchableOpacityProps } from 'react-native';
declare const FloatNaNa: (props: FloatNaNaProps) => React.JSX.Element;
export default FloatNaNa;
export interface FloatNaNaProps extends TouchableOpacityProps {
    type?: 'edit' | 'gift' | 'message' | 'calendar' | 'plus';
    actions?: any;
}
//# sourceMappingURL=FloatNaNa.d.ts.map