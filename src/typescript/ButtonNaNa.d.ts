import React from "react";
import { StyleProp, TextProps, TextStyle, ViewStyle } from "react-native";
declare const ButtonNaNa: (props: ButtonNaNaProps) => React.JSX.Element;
export default ButtonNaNa;
export interface ButtonNaNaProps extends TextProps {
    flex?: any;
    width?: any;
    left?: any;
    colorEnable?: any;
    right?: any;
    rightStyle?: any;
    title?: any;
    height?: any;
    containerStyle?: StyleProp<ViewStyle>;
    textStyle?: StyleProp<TextStyle>;
    primary?: boolean;
    default?: boolean;
    disable?: boolean;
    disabled?: boolean;
    disabled_border?: boolean;
    border?: boolean;
    type?: "primary" | "border" | "disabled" | "sub" | "border_gray" | "select" | "select_primary" | "disabled_border";
    size?: "big" | "medium" | "small";
    session?: "button" | "selection";
}
//# sourceMappingURL=ButtonNaNa.d.ts.map