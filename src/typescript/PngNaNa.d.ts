declare const PngNaNa: {
    ic_check_cir_yellow: any;
    ic_check_cir_none: any;
    ic_check_nana: any;
    ic_default_alarm: any;
    ic_down_combobox: any;
    ic_down: any;
    ic_empty: any;
    ic_float_edit: any;
    ic_float_gift: any;
    ic_float_message: any;
    ic_float_calendar: any;
    ic_info: any;
    ic_info_list_booking: any;
    ic_left_detail: any;
    ic_no_image: any;
    ic_no_avatar_friend: any;
    ic_no_avatar_white: any;
    ic_no_avatar_user: any;
    ic_plus_float: any;
    ic_setting: any;
    ic_exit: any;
};
export default PngNaNa;
//# sourceMappingURL=PngNaNa.d.ts.map