import React from "react";
import { StyleProp, ViewProps, ViewStyle } from "react-native";
export interface CheckNaNaProps extends ViewProps {
    type?: "on" | "off" | "disabled";
    _type?: "on" | "off" | "disabled";
    onPress?: any;
    size?: any;
    style?: StyleProp<ViewStyle>;
}
declare const _default: {
    Solid: ({ onPress, isSelect, style, size }: any) => React.JSX.Element;
    Def: ({ onPress, type, style, size, _type }: CheckNaNaProps) => React.JSX.Element;
};
export default _default;
//# sourceMappingURL=CheckNaNa.d.ts.map