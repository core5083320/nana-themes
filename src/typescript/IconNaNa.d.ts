import React from "react";
import { ImageProps } from "react-native";
declare const IconNaNa: (props: IconAppProps) => React.JSX.Element;
export default IconNaNa;
export interface IconAppProps extends ImageProps {
    size?: any;
    ratio?: any;
    color?: any;
    stretch?: boolean;
    cover?: boolean;
    contain?: boolean;
    size_14?: boolean;
    size_16?: boolean;
    size_20?: boolean;
    size_24?: boolean;
    size_28?: boolean;
    size_32?: boolean;
    size_36?: boolean;
    size_40?: boolean;
}
//# sourceMappingURL=IconNaNa.d.ts.map