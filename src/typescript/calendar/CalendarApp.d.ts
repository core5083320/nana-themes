import React from "react";
import { CalendarAppProps } from "./Constants";
declare const _default: {
    Select: ({ hoz, listDisabled, dayOfWeekDisabled, date, onPressItem, dateSelect, oneDay, }: CalendarAppProps) => React.JSX.Element;
    Event: ({ hoz, date, onPressItem, dateSelect, dateGreens, dateYellows, }: CalendarAppProps) => React.JSX.Element;
    Pick: ({ hoz, date, onPressItem, dateSelect, typePick, }: CalendarAppProps) => React.JSX.Element;
};
export default _default;
//# sourceMappingURL=CalendarApp.d.ts.map