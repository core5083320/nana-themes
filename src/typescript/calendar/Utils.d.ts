import moment from "moment";
export declare function isGTE(a: any, b: any): boolean;
export declare function isLTE(a: any, b: any): boolean;
export declare function month(date: any): any[];
export declare function page(date: any, firstDayOfWeek?: number, showSixWeeks?: boolean): any[];
export declare const compareDateBetweenDays: (dateFrom?: moment.Moment | any, dateTo?: moment.Moment | any) => number;
export declare const parseDateExist: ({ dateTime, dates, dateFormat, }: {
    dateTime?: moment.Moment | undefined;
    dates?: any;
    dateFormat?: any;
}) => {
    isBegin: boolean;
    isExit: boolean;
    isEnd: boolean;
};
export declare const parseDateDisabled: ({ dateTime, dates, dateFormat, }: {
    dateTime?: moment.Moment | any;
    dates?: any;
    dateFormat?: any;
}) => boolean;
export declare const parseDateData: ({ dateString, dateSelect, date, }: {
    date: moment.Moment | any;
    dateString: string;
    dateSelect?: any;
}) => {
    disabled: any;
    millisecond: any;
    day: any;
    dateTime: any;
    date: any;
    month: any;
    today: any;
    isSelect: any;
    dateFormat: any;
};
export declare const getDaysArrayBetweenTowDays: (start: moment.Moment, end: moment.Moment, format: any) => string[];
export declare const checkDateIsDisabled: ({ dateTime, listDisabled, format, dayOfWeekDisabled, }: {
    dateTime: moment.Moment;
    listDisabled: any;
    format: any;
    dayOfWeekDisabled: any;
}) => any;
export declare const partStringToTextShow: (first: any, second: any) => number;
export declare const partStringToTime: (str: any) => {
    hour: number;
    minute: number;
    second: number;
};
export declare const countSecondFromTime: (one: any, two: any) => number;
export declare const countMinuteFromTime: (first: any, second: any) => number;
export declare const parseTimeShowNotiUser: ({ start, end }: any) => string;
export declare const compareTime: (time1: any, time2: any) => 1 | -1 | 0;
export declare const compareDate: (date1: any, date2: any) => 1 | -1 | 0;
export declare const getTimeString: (_from: any, _to: any) => string | 0;
export declare const TIME_DATA: string[];
export declare const addMinute: (time: any, value: any) => string;
export declare const getDurationShowtime: (selects: any, dates: any) => string | 0;
//# sourceMappingURL=Utils.d.ts.map