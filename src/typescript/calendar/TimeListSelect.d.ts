import React from "react";
import { TimeAppProps } from "./Constants";
declare const TimeListSelect: ({ onSubmitTime, bottom, initTime, listDisabled, listHide, cta, enableRange, hideButton, onChangeTime, date, lisDisabledDateTime, oneTime, }: TimeAppProps) => React.JSX.Element;
export default TimeListSelect;
//# sourceMappingURL=TimeListSelect.d.ts.map