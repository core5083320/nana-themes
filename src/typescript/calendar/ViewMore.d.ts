import React from "react";
declare const ViewMore: ({ children, title, time, isShow, onPressShow, }: {
    children?: any;
    title?: any;
    time?: any;
    isShow?: any;
    onPressShow?: any;
}) => React.JSX.Element;
export default ViewMore;
//# sourceMappingURL=ViewMore.d.ts.map