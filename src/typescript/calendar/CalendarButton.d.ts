import React from "react";
import { TouchableOpacityProps } from "react-native";
import { CalendarAppProps } from "./Constants";
declare const CalendarButton: ({ children, onPress, onSubmitDate, oneDay, isPopup, title, header, disabled, containerStyle, onExit, ...rest }: TouchableOpacityProps & CalendarAppProps) => React.JSX.Element;
export default CalendarButton;
//# sourceMappingURL=CalendarButton.d.ts.map