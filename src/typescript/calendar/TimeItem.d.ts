import React from "react";
import { TimeItemViewProps } from "./Constants";
declare const TimeItem: ({ type, title, onPress, ...rest }: TimeItemViewProps) => React.JSX.Element;
export default TimeItem;
//# sourceMappingURL=TimeItem.d.ts.map