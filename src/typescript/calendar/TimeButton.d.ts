import React from "react";
import { TouchableOpacityProps } from "react-native";
import { TimeAppProps } from "./Constants";
declare const TimeButton: ({ children, onPress, onSubmitTime, oneTime, isPopup, title, header, disabled, containerStyle, ...rest }: TouchableOpacityProps & TimeAppProps) => React.JSX.Element;
export default TimeButton;
//# sourceMappingURL=TimeButton.d.ts.map