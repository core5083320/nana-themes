import React from "react";
import { CalendarAppProps } from "./Constants";
declare const CalendarListSelect: ({ onSubmitDate, onSelectDate, countMonth, bottom, initDate, oneDay, cta, ...rest }: CalendarAppProps) => React.JSX.Element;
export default CalendarListSelect;
//# sourceMappingURL=CalendarListSelect.d.ts.map