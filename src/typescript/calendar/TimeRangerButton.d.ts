import React from "react";
import { TouchableOpacityProps } from "react-native";
import { TimeAppProps } from "./Constants";
declare const TimeRangerButton: ({ children, onPress, onSubmitTime, oneTime, isPopup, title, header, listHide, disabled, bottom, startDate, endDate, lisDisabledDateTime, cta, initTime, enableRange, listDisabled, containerStyle, ...rest }: TouchableOpacityProps & TimeAppProps) => React.JSX.Element;
export default TimeRangerButton;
//# sourceMappingURL=TimeRangerButton.d.ts.map