import React from "react";
import { ButtonNaNaProps } from "../ButtonNaNa";
declare const ButtonCalendar: ({ title, bottom, ...rest }: ButtonNaNaProps & {
    bottom?: any;
}) => React.JSX.Element;
export default ButtonCalendar;
//# sourceMappingURL=ButtonCalendar.d.ts.map