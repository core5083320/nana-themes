import React from "react";
import { ButtonAppProps, InputAppProps } from "react-native-package-fast";
interface ConfirmModalProps {
    title?: string;
    description?: string;
    note?: string;
    input?: InputAppProps;
    left?: ButtonAppProps;
    right?: ButtonAppProps;
}
declare const _default: {
    Confirm: (params: ConfirmModalProps) => React.JSX.Element;
};
export default _default;
//# sourceMappingURL=PopupNaNa.d.ts.map