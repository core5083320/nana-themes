import React from "react";
import { InputAppProps } from "react-native-package-fast";
type InputNaNaProps = {
    disabled?: boolean;
    left?: any;
    right?: any;
    leftView?: any;
    rightView?: any;
    container?: any;
    minHeight?: any;
} & InputAppProps;
declare const _default: {
    Line: React.FC<InputNaNaProps>;
    Multi: React.FC<InputNaNaProps>;
};
export default _default;
//# sourceMappingURL=InputNaNa.d.ts.map