import React from "react";
import { ScrollViewProps } from "react-native";
interface ScrollBarNaNaProps extends ScrollViewProps {
    container?: any;
}
declare const ScrollBarNaNa: ({ children, container, style, ...rest }: ScrollBarNaNaProps) => React.JSX.Element;
export default ScrollBarNaNa;
//# sourceMappingURL=ScrollBarNaNa.d.ts.map