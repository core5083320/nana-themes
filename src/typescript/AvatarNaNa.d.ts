import React from "react";
import { ImageFastAppProps } from "react-native-package-fast";
declare const AvatarNaNa: (props: AvatarAppProps) => React.JSX.Element;
export default AvatarNaNa;
export interface AvatarAppProps extends ImageFastAppProps {
    size?: any;
    ratio?: any;
    circle?: any;
    color?: any;
    stretch?: boolean;
    source?: any;
    white?: any;
    cover?: boolean;
    contain?: boolean;
    size_80?: boolean;
    size_16?: boolean;
    size_20?: boolean;
    size_24?: boolean;
    size_28?: boolean;
    size_32?: boolean;
    size_36?: boolean;
    size_40?: boolean;
    type?: "user" | "friend" | "pet" | "other" | "user" | "alarm";
}
//# sourceMappingURL=AvatarNaNa.d.ts.map