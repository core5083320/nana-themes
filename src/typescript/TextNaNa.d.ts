import React from "react";
import { TextAppProps } from "react-native-package-fast";
declare const TextNaNa: (props: TextNaNaProps) => React.JSX.Element;
export default TextNaNa;
export interface TextNaNaProps extends TextAppProps {
    color?: any;
    hardLine?: any;
    flex?: any;
    width?: any;
    height?: any;
    title_24?: boolean;
    title_18?: boolean;
    title_16?: boolean;
    body_16?: boolean;
    body_14?: boolean;
    body_12?: boolean;
    cation_10?: boolean;
    cation_12?: boolean;
    bold?: boolean;
    medium?: boolean;
    regular?: boolean;
}
//# sourceMappingURL=TextNaNa.d.ts.map